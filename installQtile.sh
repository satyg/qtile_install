#!/bin/bash

PACMANS=(
"adobe-source-code-pro-fonts"   # OpenType font family designed for source code
"alacritty"                     # Fast, GPU-accelerated terminal emulator
"archlinux-wallpaper"           # Default wallpaper for Arch Linux
"awesome-terminal-fonts"        # Collection of monospace fonts for powerline, devicons, and more
"code"                          # Visual Studio Code - Code editing. Redefined.
"exa"                           # Modern replacement for 'ls'
"firefox"                       # Standalone web browser from Mozilla
"gimp"                          # GNU Image Manipulation Program
"git"                           # Distributed version control system
"neovim"                        # Vim-inspired text editor designed for greater extensibility and usability
"nitrogen"                      # Background browser and setter for X windows
"picom"                         # X compositor that may fix tearing issues
"pipewire"                      # Low-latency audio and video processing server
"powerline-fonts"               # Patched fonts for Powerline users
"procps-ng"                     # Utilities for monitoring your system and processes on your system
"qtile"                         # Configurable window manager written in Python
"thunar"                        # Modern file manager for the Xfce Desktop Environment
"thunderbird"                   # Free and open-source email client from Mozilla
"ttf-mononoki-nerd"
"ttf-jetbrains-mono"            # Typeface for developers designed by JetBrains
"ttf-ubuntu-font-family"        # Sans-serif typeface family designed to be a modern, humanist-style font
"unclutter"                     # Hides the mouse cursor in X after a period of inactivity
"unzip"                         # Extraction utility for archives compressed in .zip format
"wget"                          # Command-line utility for retrieving files from the web
"xorg"                          # X.Org X Window System
"xorg-xinit"                    # X.Org X server initialization program
"zsh-autosuggestions"           # Fish-like autosuggestions for zsh
"xf86-video-fbdev"              ####### This video driver is for Virtual Box!!! Change if needed!!
)

YAYS=(

)

DIRS=(
".config/alacritty"
".config/X11"
".config/qtile/images"
".config/nvim"
".config/zsh-themes"
".config/qtile/autostart"
".config/starship"
)

# Update package database and upgrade all packages to their latest versions
sudo pacman -Syu --noconfirm
# Install the packages listed in the PACMANS array, without asking for confirmation
sudo pacman -S --noconfirm "${PACMANS[@]}"

for DIR in "${DIRS[@]}"
    do
        mkdir -p $HOME/$DIR
    done

# Install AUR helper
git clone https://aur.archlinux.org/yay.git yay-git
cd yay-git; yes | makepkg -si; yay -Syyu --noconfirm; cd
rm -rf yay-git

# Install packages from AUR
yay -S --noconfirm "${YAYS[@]}"

# Creating xinitrc
head -n -5 /etc/X11/xinit/xinitrc > $HOME/.config/X11/xinitrc

# Getting Arch logo
wget https://www.svgrepo.com/show/349296/arch-linux.svg -O $HOME/.config/qtile/images/archlogo.svg

cat <<EOF >> $HOME/.config/X11/xinitrc

# Set keyboard
#setxkbmap -layout se
## Set resolution
#xrandr -s 1920x1080
## Adding transparency and no fading
#picom --no-fading-openclose &
## Restoring wallpaper
#nitrogen --restore &
## Hiding mouse pointer when inactive
#unclutter --jitter 10 --ignore-scrolling --start-hidden --fork

qtile start
EOF

cat <<EOF >> $HOME/.config/qtile/autostart/autostart.sh
#!/bin/bash

# Set keyboard
setxkbmap -layout se
# Set resolution
xrandr -s 1920x1080
# Adding transparency and no fading
picom --no-fading-openclose &
# Restoring wallpaper
nitrogen --restore &
# Hiding mouse pointer when inactive
unclutter --jitter 10 --ignore-scrolling --start-hidden --fork
EOF
chmod +x $HOME/.config/qtile/autostart/autostart.sh

# Configure Neovim

cat <<EOF > $HOME/.config/nvim/init.vim
" Set the tab width to 4 spaces
set tabstop=4
set shiftwidth=4
set expandtab

" Enable syntax highlighting
syntax enable

" Enable mouse support
set mouse=a

" Enable both absolute and relative line numbers
set number relativenumber

""" KEYBINDINGS
" Copy to clipboard
vnoremap <C-S-c> "+y

" Paste from clipboard
nnoremap <C-S-v> "+p

" Undo with CTRL+z
nnoremap <C-z> u

" Redo with CTRL+y
nnoremap <C-y> <C-r>

EOF


# Configuring Alacritty
cat <<EOF > $HOME/.config/alacritty/alacritty.yml
window:
  opacity: 0.8
  title: Alacritty
  #  decorations:
  #    None
font:
  size: 10
colors:
  # Default colors
  primary:
    background: '#1f2430'
    foreground: '#cbccc6'
    bright_foreground: '#f28779'

  # Normal colors
  normal:
    black:   '#212733' #
    red:     '#f08778' #
    green:   '#53bf97' #
    yellow:  '#fdcc60' #
    blue:    '#60b8d6' # 
    magenta: '#ec7171' #
    cyan:    '#98e6ca' #
    white:   '#fafafa' #

  # Brightened
  bright:
    black: '#686868'
    red: '#f58c7d'
    green: '#58c49c'
    yellow: '#ffd165'
    blue: '#65bddb'
    magenta: '#f17676'
    cyan: '#9debcf'
    white: '#ffffff'
EOF

# Configuring Qtile
cat <<EOF > $HOME/.config/qtile/config.py
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
import os
import subprocess
import theme

@hook.subscribe.startup_once
def autostart():
    autostartScript = os.path.expanduser("~/.config/qtile/autostart/autostart.sh")
    subprocess.call([autostartScript])

# Choose ONE of the following themes;
    # dracula
    # doomOne
    # gruvboxDark
    # nord
    # oceanicNext
    # tomorrowNight
    # macchiato
color = theme.doomOne()

# My variables
mod = "mod4"
#terminal = guess_terminal()
myBrowser = "firefox"
myTerminal = "alacritty"
myFilemanager = "thunar"

keys = [ # Modkey keybindings
    Key([mod], "e", lazy.spawn(myFilemanager), desc="Launch FileManager"),
    Key([mod], "b", lazy.spawn(myBrowser), desc="Launch Browser"),
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    Key([mod], "Return", lazy.spawn(myTerminal), desc="Launch terminal"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "r", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod], "n", lazy.window.toggle_floating(), desc="Toggle floating"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    # Modkey + Shift keybindngs
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),
    #Key([mod, "shift"], "Return", lazy.spawn("dmenu_run"),
    Key([mod, "shift"], "Return", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    # Modkey + Control keybindings
    Key([mod, "control"], "Left", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "Right", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
]

# Mouse bindings - Drag floating layouts.
mouse = [
    Drag(["shift"], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag(["shift"], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click(["shift"], "Button2", lazy.window.bring_to_front()),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

layouts = [
    layout.MonadTall(
        font = "Ubuntu",
        fontsize = 10,
        margin = 10,
        border_width=2, max_border_width=2,
        border_focus=color[7], border_normal=color[10]
        ),
    layout.Columns(
        border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4
        ),
    layout.Max(),
    layout.Floating(
        border_width=2, max_border_width=2,
        border_focus="#ff79c6", border_normal="#6272a4"
        )
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="Source Code Pro",
    fontsize=13,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                # widget.CurrentLayout(),
                widget.Image(
                    background=color[0],
                    filename='~/.config/qtile/images/archlogo.svg',
                    scale = "False",
                    ),
                widget.GroupBox(
                    #hide_unused=True,
                    fontsize=15,
                    background=color[0],
                    highlight_method="line",
                    active=color[7],
                    inactive=color[10],
                    other_screen_border=color[8],
                    this_current_screen_border=color[4]
                    ),
                widget.TextBox("",
                    foreground=color[11],
                    background=color[0],
                    fontsize=20
                    ),
                widget.Prompt(
                    foreground=color[5],background=color[0]
                    ),
                widget.WindowName(
                    background=color[0],
                    foreground=color[6]
                    ),
                #widget.Chord(
                #    chords_colors={
                #        "launch": ("#ff0000", "#ffffff"),
                #    },
                #    name_transform=lambda name: name.upper(),
                #),
                # widget.TextBox("default config", name="default"),
                # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Systray(
                    background=color[0],
                    foreground=color[7],
                    icon_size=10
                    ),
                widget.Volume(
                    background=color[0],
                    foreground=color[7],
                    fmt='Vol: {}',
                    ),
                widget.TextBox("",
                        foreground=color[10],
                        background=color[0],
                        fontsize=10
                    ),
                widget.CheckUpdates(
                    background=color[0],
                    distro="Arch_checkupdates",
                    display_format="Updates: {updates} ",
                    update_interval=900,
                    no_update_string='No updates',
                    colour_have_updates=color[3],
                    colour_no_updates=color[4],
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(myTerminal + ' -e sudo pacman -Syu')}
                    ),
                widget.TextBox("",
                        foreground=color[10],
                        background=color[0],
                        fontsize=10
                    ),
                widget.Clock(
                    foreground=color[5],background=color[0],
                    format="%y-%m-%d %A %H:%M"
                    ),
                widget.TextBox("",
                        foreground=color[10],
                        background=color[0],
                        fontsize=10
                    ),
                widget.QuickExit(
                    background=color[0]
                    ),
            ],
            24,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
]


dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
	Match(title="Calculator"),
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
EOF

# Configuring themes for Qtile
cat <<EOF > $HOME/.config/qtile/theme.py
import os

# Dracula theme
def dracula():
    color = ["#282a36",     # Background 
            "#f8f8f2",      # Foreground 
            "#000000",      # Black 
            "#ff5555",      # Red 
            "#50fa7b",      # Green 
            "#f1fa8c",      # Yellow       
            "#bd93f9",      # Purple 
            "#ff79c6",      # Pink 
            "#8be9fd",      # Cyan 
            "#bfbfbf",      # LightGray 
            "#4d4d4d",      # DarkGray
            "#ff6e67",      # Orange
            "#5af78e",      # PaleGreen
            "#f4f99d",      # PaleYellow
            "#caa9fa",      # PalePurple
            "#ff92d0",      # PalePink
            "#9aedfe",      # PaleCyan
            "#e6e6e6"]      # White
    return color

# Doom-One
def doomOne():
    color = ["#282c34",
            "#bbc2cf",
            "#1c1f24",
            "#ff6c6b",
            "#98be65",
            "#da8548",
            "#51afef",
            "#c678dd",
            "#5699af",
            "#202328",
            "#5b6268",
            "#da8548",
            "#4db5bd",
            "#ecbe7b",
            "#3071db",
            "#a9a1e1",
            "#46d9ff",
            "#dfdfdf"]
    return color

# GruvBox-Dark
def gruvboxDark():
    color = ["#282828",
            "#ebdbb2",
            "#282828",
            "#cc241d",
            "#98971a",
            "#d79921",
            "#458588",
            "#b16286",
            "#689d6a",
            "#a89984",
            "#928374",
            "#fb4934",
            "#b8bb26",
            "#fabd2f",
            "#83a598",
            "#d3869b",
            "#8ec07c",
            "#ebdbb2"]
    return color

# Nord
def nord():
    color = ["#2E3440",
            "#D8DEE9",
            "#3B4252",
            "#BF616A",
            "#A3BE8C",
            "#EBCB8B",
            "#81A1C1",
            "#B48EAD",
            "#88C0D0",
            "#E5E9F0",
            "#4C566A",
            "#BF616A",
            "#A3BE8C",
            "#EBCB8B",
            "#81A1C1",
            "#B48EAD",
            "#8FBCBB",
            "#ECEFF4"]
    return color

# OceanicNext
def oceanicNext():
    color = ["#1b2b34",
            "#d8dee9",
            "#29414f",
            "#ec5f67",
            "#99c794",
            "#fac863",
            "#6699cc",
            "#c594c5",
            "#5fb3b3",
            "#65737e",
            "#405860",
            "#ec5f67",
            "#99c794",
            "#fac863",
            "#6699cc",
            "#c594c5",
            "#5fb3b3",
            "#adb5c0"]
    return color

# TomorrowNight
def tomorrowNight():
    color = ["#1d1f21",
            "#c5c8c6",
            "#1d1f21",
            "#cc6666",
            "#b5bd68",
            "#e6c547",
            "#81a2be",
            "#b294bb",
            "#70c0ba",
            "#373b41",
            "#666666",
            "#ff3334",
            "#9ec400",
            "#f0c674",
            "#81a2be",
            "#b77ee0",
            "#54ced6",
            "#282a2e"]
    return color

# Macchiato theme
def macchiato():
    color = ["#181926",     # Background 
            "#f4dbd6",      # Foreground 
            "#181926",      # Black 
            "#ed8796",      # Red 
            "#a6da95",      # Green 
            "#eed49f",      # Yellow       
            "#c6a0f6",      # Purple 
            "#f5bde6",      # Pink 
            "#7dc4e4",      # Cyan 
            "#939ab7",      # LightGray 
            "#5b0078",      # DarkGray
            "#f5a97f",      # Orange
            "#8bd5ca",      # PaleGreen
            "#eed49f",      # PaleYellow
            "#b7bd48",      # PalePurple
            "#f5bde6",      # PalePink
            "#91d7e3",      # PaleCyan
            "#f0c6c6"]      # White
    return color
EOF

# Editing grub
#sudo sed -i s'/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet"/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash nomodeset"/' /etc/default/grub
#sudo sed -i s'/GRUB_GFXMODE=auto/GRUB_GFXMODE=1600x900/' /etc/default/grub
#sudo grub-mkconfig -o /boot/grub/grub.cfg

# Configuring .zshrc
cat <<EOF > $HOME/.zshrc
### EXPORT
# Set the terminal type to xterm-256color
export TERM="xterm-256color"
# Set the default text editor to neovim
export EDITOR="nvim"

### PATH
# Add ~/.bin to the PATH if it exists
if [ -d "\$HOME/.bin" ]; then
    PATH="\$HOME/.bin:\$PATH"
fi

# Add ~/.local/bin to the PATH if it exists
if [ -d "\$HOME/.local/bin" ]; then
    PATH="\$HOME/.local/bin:\$PATH"
fi

### SETTING OTHER ENVIRONMENT VARIABLES
# Set the XDG_CONFIG_HOME environment variable to ~/.config if it is not already set
if [ -z "\$XDG_CONFIG_HOME" ]; then
    export XDG_CONFIG_HOME="\$HOME/.config"
fi

# Set the XDG_DATA_HOME environment variable to ~/.local/share if it is not already set
if [ -z "\$XDG_DATA_HOME" ]; then
    export XDG_DATA_HOME="\$HOME/.local/share"
fi

# Set the XDG_CACHE_HOME environment variable to ~/.cache if it is not already set
if [ -z "\$XDG_CACHE_HOME" ]; then
    export XDG_CACHE_HOME="\$HOME/.cache"
fi

### PLUGINS
# Enable the git and zsh-autosuggestions plugins
plugins=(git zsh-autosuggestions, zsh-syntax-highlighting)
# Source the zsh-autosuggestions plugin
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Set the number of history entries to keep
SAVEHIST=1000
# Set the location of the zsh history file
HISTFILE=\$HOME/.zsh_history

### ALIASES
# Set an alias for vim to use neovim instead
alias vim='nvim'

# Set aliases for navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Use "exa" instead of "ls" for listing directory contents
alias ls='exa -al --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias l.='exa -a | egrep "^\."'

eval "\$(starship init zsh)"

### STARTING QTILE
# Start the Qtile window manager if the current terminal is tty1 and Qtile is not already running
if [[ "$(tty)" = "/dev/tty1" ]]; then
    pgrep qtile || startx "\$XDG_CONFIG_HOME/X11/xinitrc"
fi
EOF

curl -sS https://starship.rs/install.sh | sh -s -- -y
curl -L https://starship.rs/presets/toml/pure-preset.toml -o $HOME/.config/starship/starship.toml
starship preset pure-preset -o $HOME/.config/starship/starship.toml
source $HOME/.zshrc
